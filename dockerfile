#Use Official Python runtime
FROM python:bookworm

#Set working dir
WORKDIR /app

#add current dir contents to container at /app
ADD . /app

#Install dependencies
RUN pip install mkdocs-material
RUN pip install mkdocs-glightbox

#Make port 8000 available to the world outside this container
EXPOSE 8000

#Run mkdocs serve
CMD ["mkdocs", "serve", "--dev-addr=0.0.0.0:8000"]
