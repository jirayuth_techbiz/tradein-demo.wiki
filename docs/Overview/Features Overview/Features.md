## Feature Overview (WIP)

ตัวแอพจะมีหน้าหลักๆ อยู่ทั้งหมด 4 หน้าใหญ่และ 3 หน้าย่อยด้วยกัน สำหรับการ Implement ของแต่ละหน้า สามารถตามลิงค์ของแต่ละหน้าไปได้เลย

| Image | Description |
|:---:|:---:|
| ![Home Page](home-page.jpg){align=left} | <div style="height: 180px;"></div> <H3>Home Page</H3> นี่คือหน้าแรกของแอพ ใบหน้าหลักจะมีรายละเอียดของ Hardware ที่กำลังใช้เปิดแอพอยู่ พร้อมรายละเอียดเบื้องต้นเกี่ยวกับความจุเครื่อง |
| ![Preparation Page](Prepare_1.jpg){aligh = left} &nbsp; ![Preparation Page](Prepare_2.jpg){align=left} | <div style="height: 550px;"></div> <H3>Preparation Page</H3>หน้าเตรียมพร้อมก่อนเริ่มการทดสอบซึ่งจะทำการขออนุญาติ User เพื่อเข้าถึงสิทธิ์การใช้งานแอพในส่วนของ Hardware ด้านต่างๆ อาทิกล้อง, ไมโครโฟน และอื่นๆ ที่จำเป็น | 
| ![Test Page](Test_Page_Sample.jpg){align=left} | <div style="height: 180px;"></div> <H3>Test Page (Automatic)</H3> ตัวอย่างหน้าตาของหน้า Test กรณีที่การทดสอบเป็นไปโดยอัติโนมัติ เช่น Wi-fi ที่ไม่ต้องรอผู้ใช้งานส่งข้อมูลเข้ามา สามารถเรียกและตรวจสอบผลลัพธ์ได้เองทันที |
| ![Test Page](Test_Page_Sample_Manual_1.jpg){align=left} | <div style="height: 150px;"></div> <H3>Test Page (Manual)</H3> ตัวอย่างหน้าตาของหน้า Test กรณีที่การทดสอบต้องการให้ผู้ใข้งาน Interact กับเครื่องเพื่อทำการทดสอบให้เสร็จสมบูรณ์ โดยส่วนมากจะเป็น Hardware ภายนอกที่สามารถมองเห็นภาพผลได้ อาทิกล้อง หรือหน้าจอ |


