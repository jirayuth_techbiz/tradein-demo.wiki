## Overview

แอพ Trade-in ตัวนี้เป็นแอพพลิเคชั่นสำหรับระบบปฏิบัติการมือถือระบบ iOS และ Android สำหรับใช้งานร่วมกับเว็บไซต์ Trade in หลังบ้าน 

Feature หลักของตัวแอพนั้นคือระบบทดสอบปฏิบัติการ Hardware สำหรับตัวมือถือ เป็นการเช็คอุปกรณ์ภายในเครื่องที่ไม่สามารถตรวจสอบหรือพบได้ด้วยตาเปล่า ซึ่งจะช่วย Streamline Process ในส่วนของการประเมินราคาเครื่อง และลดปริมาณงานหลังบ้านลง ทั้งยังสร้่างความสะดวกสบายให้กับผู้ที่ต้องการจะนำมือถือเก่าของตนเองมาเทรดอิน เพื่อแลกเป็นส่วนลดซื้อมือถือเครื่องใหม่ ให้เกิดความรวดเร็วและเรียบง่ายมากขึ้น

---

## Goals

- แอพจะต้องสามารถเช็คได้ว่าตัวมือถือ Hardware นั้นคือเครื่องอะไร ความจุเท่าไหร่
- แอพจะต้องสามารถตรวจสอบการทำงานของ Hardware บนตัวมือถือได้ไม่ว่าจะเป็น Platform iOS หรือ Android
- แอพจะต้องสามารถเก็บข้อมูลการตรวจสอบการทำงานและส่งข้อมูลมายัง Server หลังบ้านเพื่อประมวลผลข้อมูลได้

---

## Tech Stack

- Flutter Framework
- ASP.Net Core (Backend API)

## Implementation

ตัวแอพใช้ 2 Pattern หลักในการบริหาร State

### Riverpod Pattern

แยก Business Logic กับ View ออกจากกันด้วย State Management ชื่อ ![Riverpod](https://riverpod.dev/) โดยใช้ State Provider เป็นตัวเก็บ Business Logic และจ่ายข้อมูลในเวลาเดียวกัน โดยที่ไม่ต้องใช้ Service Locator หรือ Dependency Injection ใดๆ <br><br> เหมาะกับ Logic ที่ต้อง Interact ภายในเครื่องโดยตรงอาทิ

- ดึงสถานะ UI อาทิ Switch เปิด-ปิด หรือการตอบสนองอื่นๆ จาก User
- ดึงข้อมูลจากหน้าอื่นๆ เข้ามาแสดงผล

ข้อดีหลักของ Riverpod คือความเร็วในการ Implement และการ Test ที่สามารถ Mock และทดสอบได้อย่างรวดเร็ว รวมไปถึงการทำงานร่วมกับ UI ที่มีประสิทธิภาพที่สูงมาก สามารถตอบสนอง User Interaction ได้แบบ Real-Time โดยไม่มีผลกระทบต่อ Performance เลย แต่ข้อเสียคือไม่เหมาะกับ Logic ที่ซับซ้อนหลายขั้นตอนเพราะอาจจะทำให้ Code อ่านยากขึ้น ส่วนใหญ่ Pattern นี้จึงใช้เฉพาะในหน้าทดสอบเครื่องอย่างเดียวที่ไม่ต้องการ Implementation ที่ซับซ้อนจนเกินไป 

#### Overview Flow  

Folder Structure จะออกเป็นแนวราบง่ายๆ ไม่มีวงจรซับซ้อน

``` mermaid
graph TB
    A[Features]
    A --> B[State]
    A --> C[View]
```
ส่วนระบบการทำงานภายในแอพจะ Flow ตามนี้

    - กรอบสี่เหลื่ยมจัตุรัส ใช้เป็นตัวแทน Object หลัก ในที่นี้คือ User, UI
    - กรอบสี่เหลี่ยมมน ใช้เป็นตัวแทน Abstract Object ในที่นี้คือปฏิสัมพันธ์และข้อมูลต่างๆ
    - กรอบสี่เหลี่ยมคางหมู ใช้เป็นตัวแทนปฏิบัติการตัวกลางภายใน ในที่นี้คือ State Provider

``` mermaid
graph TB
  A[User] --> B(Interaction)
  B --> C[UI]
  D(Business Logic) --> E{State Provider}
  C --> E
  E --> F(Data)
  F --> C
  C --> A
```

### Repository Pattern

เพื่อให้การทดสอบ API สามารถทำได้โดยง่ายและราบรื้น ทุกหน้าที่จำเป็นจะต้องใช้ API จึงใช้ Repository Pattern เข้ามาแยก Business Logic กับหน้า View ออกจากกัน แยกหน้าออกอย่างชัดเจนระหว่าง Repository กับ Business Logic 

โดย Repository จะเป็นตัว Deserialization JSON ออกมาเป็น Model Object ให้ตัวแอพสามารถเรียกไปใช้งานได้ง่าย พร้อมยังเป็นพื้นที่เก็บเส้น API ต่างๆ รอให้ตัว Business Logic มาดึงเอาไปใช้ในปฏิบัติการของตนเอง โดยใช้ Riverpod มาทำเป็น Service Locator สำหรับตัว Repository 

#### Overview Flow  

Folder Structure จะใช้ตาม ![Flow บทความนี้](https://codewithandrea.com/articles/flutter-repository-pattern/) ซึ่งอธิบายการเขียนรูปแบบ Repository Pattern เอาไว้

``` mermaid
graph TB
    A[Features]
    A --> B[App]
    B --> F("Interface (Abstract Class)")

    A --> C[Data]
    C --> G(Repository)

    A --> D[Model]
    D --> H(Model Object)

    A --> E[View]
    E --> J(State)
    E --> I(View Page)
```
ส่วนระบบการทำงานภายในแอพจะ Flow ตามนี้

    - กรอบสี่เหลื่ยมจัตุรัส ใช้เป็นตัวแทน Object หลัก ในที่นี้คือ User, UI
    - กรอบสี่เหลี่ยมมน ใช้เป็นตัวแทน Abstract Object ในที่นี้คือปฏิสัมพันธ์และข้อมูลต่างๆ
    - กรอบสี่เหลี่ยมคางหมู ใช้เป็นตัวแทนปฏิบัติการตัวกลางภายใน ในที่นี้คือ State Provider

``` mermaid
graph TB
  A[User] --> B[UI]
  B --> C{State Provider}
  C --> D(Service)
  D --> E{Repository}
  
  F[API] --> G("Interface (Abstract Class)")
  G --> E
  E --> D
  D --> C
  C --> B
  B --> A
```
